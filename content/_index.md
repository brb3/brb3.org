---
title: Home
---

<div class="h-card">
<img
src="https://s.gravatar.com/avatar/22793c7a69b55e363052c07763d1e044?s=160&r=g"
style="max-width:15%;min-width:40px;float:right;" alt="Bobby"
class="u-logo u-photo" />

<h1>brb3</h1>

<div class="p-note">
<p>
My name is <span class="p-name"><span class="p-given-name">Bobby</span> <span
class="p-family-name">Burden</a> III</a>. I am a Software Developer living in Ringgold, Georgia
with my wife Nicki. The easiest way to get in touch with me is to shoot me an <a
href="mailto:bobby@brb3.org" rel="me" class="u-email">email</a>.
</p>

<p>
I work for UPS i-parcel where I am a “BIA Intermedia Development Manager”.
That’s a fancy way of saying that I work on developing integrations between
ecommerce platforms and the UPS i-parcel suite of services.
</p>

<p>
In my work and personal projects, I focus primarily on PHP, Ruby, and ecommerce.
My personal website is <a href="https://brb3.org" rel="me" class="u-url u-uid">here</a>.
</p>

<p>
In my free time, I am an amateur radio operator and use the callsign W1BRB.
Also, I run a community of local developers called devanooga. You’re welcome to
come join us on slack!
</p>

</div>
</div>

<hr/>

### Blog
